﻿using System.Collections.Generic;
using WebApiTransacctions.Datos.Models;

namespace WebApiTransacctions.Repositorios.Interfaces
{
    interface ITransacctionFakeRepository
    {
        List<Transacction> _transacctionsList { get; set; }
        List<Transacction> Get();
        Transacction Find(string sku);
    }
}
