﻿using System.Collections.Generic;
using WebApiTransacctions.Datos.Models;


namespace WebApiTransacctions.Repositorios.Interfaces
{

    public interface ITransacctionRepository
    {

        List<Transacction> _transacctionsList { get; set; }
        List<Transacction> Get();
        Transacction Find(string sku);
    }
}
