﻿using System.Collections.Generic;
using WebApiTransacctions.Datos.Models;


namespace WebApiTransacctions.Repositorios.Interfaces
{
    interface IRateRepository 
    {
        List<Rate> _ratesList { get; set; }
        List<Rate> Get();
        Rate Find(string from, string to);
    }
}
