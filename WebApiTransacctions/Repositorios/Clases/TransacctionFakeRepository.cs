﻿using System.Collections.Generic;
using System.Linq;
using WebApiTransacctions.Datos.Models;
using WebApiTransacctions.Infraestructura.Excepciones.Repositorios;
using WebApiTransacctions.Infraestructura.Log;
using WebApiTransacctions.Repositorios.Interfaces;

namespace WebApiTransacctions.Repositorios.Clases
{
    public class TransacctionFakeRepository : ITransacctionFakeRepository
    {
        private LogPrueba _log;
        public List<Transacction> _transacctionsList { get; set; } 
        = new List<Transacction>
        {
            new Transacction("T2006", "USD", 10.00m),
            new Transacction("M2007", "CAD", 34.57m),
            new Transacction("R2008", "USD", 17.95m),
            new Transacction("T2006", "EUR", 7.63m),
            new Transacction("B2009", "USD", 21.23m)
        };

        private static TransacctionFakeRepository _instancia;

        public static TransacctionFakeRepository Instance
        {
            get
            {
                if (_instancia == null)
                    _instancia = new TransacctionFakeRepository();

                return _instancia;
            }
        }

        public Transacction Find(string sku)
        {
            try
            {
                return _transacctionsList.Where(x => x._sku == sku).FirstOrDefault();
            }
            catch (TransacctionNotFoundException e)
            {

                _log.EscribeExcepcion(e);

                throw new TransacctionNotFoundException(sku);
            }

        }

        public List<Transacction> Get()
        {
            try
            {
                return _transacctionsList.ToList();
            }
            catch (TransacctionListNullException e)
            {
                _log.EscribeExcepcion(e);

                throw new TransacctionListNullException();

            }
        }
    }
}