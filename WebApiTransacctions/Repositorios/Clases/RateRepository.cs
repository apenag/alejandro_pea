﻿using System.Collections.Generic;
using System.Linq;
using WebApiTransacctions.Datos.Models;
using WebApiTransacctions.Infraestructura.Excepciones.Repositorios;
using WebApiTransacctions.Infraestructura.Log;
using WebApiTransacctions.Repositorios.Interfaces;
using WebApiTransacctions.Servicios.Interfaces;

namespace WebApiTransacctions.Repositorios.Clases
{
    public class RateRepository : IRateRepository
    {
        private LogProduccion _log;
        private IReadJsonService<Rate> _jsonReader { get; set; }
        private IJsonConverterApi<Rate> _jsonConverter { get; set; }
        public List<Rate> _ratesList { get; set; }

        public RateRepository() { }

        public RateRepository(IReadJsonService<Rate> jsonReader, IJsonConverterApi<Rate> jsonConverter)
        {
            _jsonConverter = jsonConverter;
            _jsonReader = jsonReader;
        }


        public Rate Find(string from, string to)
        {

            try
            {
                _ratesList = _jsonConverter.Deserialize(_jsonReader.Read());
                var rate = _ratesList.Where(x => x._from == from && x._to == to).FirstOrDefault();

                return rate;
            }
            catch (RateNotFoundException e)
            {
                _log.EscribeExcepcion(e);
                throw new RateNotFoundException(from, to);

            }

        }

        public List<Rate> Get()
        {
            try
            {
                _ratesList = _jsonConverter.Deserialize(_jsonReader.Read());

                return _ratesList;
            }
            catch (RateListNullException e)
            {

                _log.EscribeExcepcion(e);

                throw new RateListNullException();
            }

        }
    }
}