﻿using System.Collections.Generic;
using System.Linq;
using WebApiTransacctions.Datos.Models;
using WebApiTransacctions.Infraestructura.Excepciones.Repositorios;
using WebApiTransacctions.Infraestructura.Log;
using WebApiTransacctions.Repositorios.Interfaces;

namespace WebApiTransacctions.Repositorios.Clases
{
    public class RateFakeRepository : IRateFakeRepository
    {
        private LogPrueba _log;
        public List<Rate> _ratesList { get; set; }
        = new List<Rate> 
        {
            new Rate("EUR", "USD", 1.359),
            new Rate("CAD", "EUR", 0.732),
            new Rate("USD", "EUR", 0.736),
            new Rate("EUR", "CAD", 1.366),
        };
        private static RateFakeRepository _instancia;

        public static RateFakeRepository Instance
        {
            get
            {
                if (_instancia == null)
                    _instancia = new RateFakeRepository();

                return _instancia;
            }
        }

        public Rate Find(string to, string from)
        {
            try
            {
                return _ratesList.Where(x => x._to == to && x._from == from).FirstOrDefault();
            }
            catch (RateNotFoundException e)
            {

                _log.EscribeExcepcion(e);
                throw new RateNotFoundException(from,to);

            }

        }

        public List<Rate> Get()
        {
            try
            {
                return _ratesList.ToList();
            }
            catch (RateListNullException e)
            {

                _log.EscribeExcepcion(e);
                throw new RateListNullException();

            }
        }
    }
}