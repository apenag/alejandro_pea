﻿using System.Collections.Generic;
using System.Linq;
using WebApiTransacctions.Datos.Models;
using WebApiTransacctions.Infraestructura.Excepciones.Repositorios;
using WebApiTransacctions.Infraestructura.Log;
using WebApiTransacctions.Repositorios.Interfaces;
using WebApiTransacctions.Servicios.Interfaces;

namespace WebApiTransacctions.Repositorios.Clases
{
    public class TransacctionRepository : ITransacctionRepository
    {

        IReadJsonService<Transacction> _jsonReader { get; set; }
        IJsonConverterApi<Transacction> _jsonConverter { get; set; }
        public List<Transacction> _transacctionsList { get; set; }
        private LogProduccion _log;

        public TransacctionRepository() { }

        public TransacctionRepository(IReadJsonService<Transacction> jsonReader, IJsonConverterApi<Transacction> jsonConverter)
        {
            _jsonConverter = jsonConverter;
            _jsonReader = jsonReader;
        }


        public Transacction Find(string sku)
        {
            try
            {
                _transacctionsList = _jsonConverter.Deserialize(_jsonReader.Read());
                var trans = _transacctionsList.Where(x => x._sku == sku).FirstOrDefault();

                return trans;
            }
            catch (TransacctionNotFoundException e)
            {
                _log.EscribeExcepcion(e);

                throw new TransacctionNotFoundException(sku);

            }

        }

        public List<Transacction> Get()
        {
            try
            {
                _transacctionsList = _jsonConverter.Deserialize(_jsonReader.Read());

                return _transacctionsList;
            }
            catch (TransacctionListNullException e)
            {
                _log.EscribeExcepcion(e);

                throw new TransacctionListNullException();


            }

        }
    }
}