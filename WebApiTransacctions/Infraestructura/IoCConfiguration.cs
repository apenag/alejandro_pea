﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using log4net;
using System;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using WebApiTransacctions.Datos.Interfaces;
using WebApiTransacctions.Datos.Models;
using WebApiTransacctions.Repositorios.Clases;
using WebApiTransacctions.Repositorios.Interfaces;
using WebApiTransacctions.Servicios.Clases;
using WebApiTransacctions.Servicios.Interfaces;

namespace WebApiTransacctions.Infraestructura
{
    public class IoCConfiguration
    {
        public static IContainer Container { get; set; }

        public static T GetInstance<T>()
        {
            return Container.Resolve<T>();
        }

        public static void Configure()
        {
            var builder = new ContainerBuilder();


            RegisterRepositories(builder);
            RegisterServices(builder);
            RegisterControllers(builder);

            Container = builder.Build();

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(Container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(Container));


        }

        private static void RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(JsonConverterApi<>)).As(typeof(IJsonConverterApi<>)).SingleInstance();
            builder.RegisterGeneric(typeof(JsonConverterLocal<>)).As(typeof(IJsonConverterLocal<>)).SingleInstance();
            builder.RegisterGeneric(typeof(ReadJsonService<>)).As(typeof(IReadJsonService<>)).SingleInstance();
        }

        private static void RegisterRepositories(ContainerBuilder builder)
        {
            var valor = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (valor == "Develoment")
            {

                builder.RegisterType<RateFakeRepository>().As<IRateFakeRepository>().SingleInstance();
                builder.RegisterType<TransacctionFakeRepository>().As < ITransacctionFakeRepository>().SingleInstance();
                builder.RegisterType<Log.LogPrueba>().As<ILog>().SingleInstance();
            }
            else
            {
                builder.RegisterType<RateRepository>().As<IRateRepository>().SingleInstance();
                builder.RegisterType<TransacctionRepository>().As<ITransacctionRepository>().SingleInstance();
                builder.RegisterType<Log.LogProduccion>().As<ILog>().SingleInstance();
            }

            builder.RegisterType<Rate>().As<IRate>().SingleInstance();
            builder.RegisterType<Transacction>().As<ITransacction>().SingleInstance();
        }

        private static void RegisterControllers(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
        }
    }
}