﻿using System;

namespace WebApiTransacctions.Infraestructura.Excepciones.Repositorios
{

    public class RateListNullException : ApplicationException
    {
        public RateListNullException() : base("La lista de rates está vacía") { }
        public RateListNullException(Exception innerException) : base("La lista de rates está vacía", innerException) { }
    }
}