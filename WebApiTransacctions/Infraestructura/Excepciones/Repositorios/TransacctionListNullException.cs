﻿using System;

namespace WebApiTransacctions.Infraestructura.Excepciones.Repositorios
{
    public class TransacctionListNullException : ApplicationException
    {
        public TransacctionListNullException() : base("La lista de transacciones está vacía") { }
        public TransacctionListNullException(Exception innerException) : base("La transacciones de rates está vacía", innerException) { }
    }
}