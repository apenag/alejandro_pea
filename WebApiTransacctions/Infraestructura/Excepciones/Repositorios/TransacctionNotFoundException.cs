﻿using System;

namespace WebApiTransacctions.Infraestructura.Excepciones.Repositorios
{
    public class TransacctionNotFoundException : ApplicationException
    {
        public TransacctionNotFoundException(string transaccion) : base(String.Format("La transaccion {0} no se ha podido encontrar", transaccion)) { }
        public TransacctionNotFoundException(string transaccion, Exception innerException) : base(String.Format("La transaccion {0} no se ha podido encontrar", transaccion), innerException) { }
    }
}