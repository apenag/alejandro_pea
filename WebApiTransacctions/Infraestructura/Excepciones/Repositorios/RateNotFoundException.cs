﻿using System;

namespace WebApiTransacctions.Infraestructura.Excepciones.Repositorios
{

    public class RateNotFoundException : ApplicationException
    {
        public RateNotFoundException(string from, string to) : base(String.Format("El cambio de {0} a {1} no se ha encontrado", from,to)) { }
        public RateNotFoundException(string from,string to, Exception innerException) : base(String.Format("El cambio de {0} a {1} no se ha encontrado", from, to), innerException) { }
    }
}