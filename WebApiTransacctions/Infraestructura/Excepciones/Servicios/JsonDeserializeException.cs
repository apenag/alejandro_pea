﻿using System;

namespace WebApiTransacctions.Infraestructura.Excepciones.Servicios
{

    public class JsonDeserializeException : ApplicationException
    {
        public JsonDeserializeException() : base("No se puede deserializar archivo JSON") { }
        public JsonDeserializeException(Exception innerException) : base("No se puede deserializar archivo JSON", innerException) { }
    }

}