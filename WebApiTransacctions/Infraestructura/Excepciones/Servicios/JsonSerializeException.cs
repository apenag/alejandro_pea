﻿using System;

namespace WebApiTransacctions.Infraestructura.Excepciones.Servicios
{

    public class JsonSerializeException : ApplicationException
    {
        public JsonSerializeException() : base("No se puede serializar el archivo a JSON") { }
        public JsonSerializeException(Exception innerException) : base("No se puede serializar el archivo a JSON", innerException) { }
    }

}