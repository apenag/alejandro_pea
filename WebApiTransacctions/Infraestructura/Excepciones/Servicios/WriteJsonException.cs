﻿using System;

namespace WebApiTransacctions.Infraestructura.Excepciones.Servicios
{

    public class WriteJsonException : ApplicationException
    {
        public WriteJsonException() : base("No se puede escribir el archivo JSON") { }
        public WriteJsonException(Exception innerException) : base("No se puede escribir el archivo JSON", innerException) { }
    }
}