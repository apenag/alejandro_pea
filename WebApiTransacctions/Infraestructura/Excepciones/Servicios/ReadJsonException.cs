﻿using System;

namespace WebApiTransacctions.Infraestructura.Excepciones.Servicios
{

    public class ReadJsonException : ApplicationException
    {
        public ReadJsonException() : base("No se puede leer el archivo JSON") { }
        public ReadJsonException(Exception innerException) : base("No se puede leer el archivo JSON", innerException) { }
    }

}