﻿using System;

namespace WebApiTransacctions.Infraestructura.Excepciones.Servicios
{

    public class SumNotCorrectException : ApplicationException
    {
        public SumNotCorrectException() : base("Ha habido un problema al sumar el total") { }
        public SumNotCorrectException(Exception innerException) : base("Ha habido un problema al sumar el total", innerException) { }
    }

}