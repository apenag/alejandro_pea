﻿using System;
using Autofac;
using log4net;


namespace WebApiTransacctions.Infraestructura
{
    public abstract class ApplicationException : Exception
    {
            readonly ILog Log;

            public ApplicationException() : this("Mensaje no definido")
            {

            }

            public ApplicationException(String mensaje) : base(mensaje)
            {
                using (var scope = IoCConfiguration.Container.BeginLifetimeScope())
                {
                    var Log = scope.Resolve<ILog>();
                    //Log.EscribeExcepcion(this);
                }
            }

            public ApplicationException(String mensaje, Exception innerException) : base(mensaje, innerException)
            {

            }
    }


}