﻿namespace WebApiTransacctions.Servicios.Interfaces
{
    public interface IReadJsonService<T>
    {
        string Read();
    }
}
