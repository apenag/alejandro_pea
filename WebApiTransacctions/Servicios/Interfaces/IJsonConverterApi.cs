﻿using System.Collections.Generic;

namespace WebApiTransacctions.Servicios.Interfaces
{
    public interface IJsonConverterApi<T>
    {
        string Serialize(List<T> list);
        List<T> Deserialize(string json);
    }
}
