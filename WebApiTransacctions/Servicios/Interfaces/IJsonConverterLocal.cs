﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiTransacctions.Servicios.Interfaces
{
    interface IJsonConverterLocal<T>
    {
        string Serialize(List<T> list);
        List<T> Deserialize(string path);
    }
}
