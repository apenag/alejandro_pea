﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using WebApiTransacctions.Infraestructura.Excepciones.Servicios;
using WebApiTransacctions.Infraestructura.Log;
using WebApiTransacctions.Servicios.Interfaces;

namespace WebApiTransacctions.Servicios.Clases
{
    public class JsonConverterLocal<T> : IJsonConverterLocal<T> where T : class
    {
        private LogProduccion _log;

        public List<T> Deserialize(string path)
        {
            try
            {
                var json = File.ReadAllText(path);

                List<T> listDeserializada = JsonConvert.DeserializeObject<List<T>>(json);

                return listDeserializada;
            }
            catch (JsonDeserializeException e)
            {
                _log.EscribeExcepcion(e);
                throw new JsonDeserializeException();

            }

        }

        public string Serialize(List<T> list)
        {
            try
            {
                string json = JsonConvert.SerializeObject(list, Formatting.Indented);

                return json;
            }
            catch (JsonSerializeException e)
            {
                _log.EscribeExcepcion(e);
                throw new JsonSerializeException();

            }

        }
    }
}