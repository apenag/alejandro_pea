﻿using System;
using System.Configuration;
using System.Net;
using WebApiTransacctions.Datos.Models;
using WebApiTransacctions.Infraestructura.Excepciones.Servicios;
using WebApiTransacctions.Infraestructura.Log;
using WebApiTransacctions.Servicios.Interfaces;

namespace WebApiTransacctions.Servicios.Clases
{
    public class ReadJsonService<T> : IReadJsonService<T> where T : class
    {
        private LogProduccion _log;
        public string Read()
        {
            string json = string.Empty;
            try
            {
                using (WebClient cliente = new WebClient())
                {
                    if (typeof(Rate) == typeof(T))
                    {
                        json = cliente.DownloadString(ConfigurationManager.AppSettings["direccionRates"]);
                    }
                    else {
                        json = cliente.DownloadString(ConfigurationManager.AppSettings["direccionTransacctions"]);
                    }
                  
                }

                return json;
            }
            catch (ReadJsonException e)
            {
                _log.EscribeExcepcion(e);
                throw new ReadJsonException(); 
            }

        }
    }
}