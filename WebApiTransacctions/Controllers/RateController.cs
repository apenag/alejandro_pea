﻿using System.Collections.Generic;
using System.Web.Http;
using WebApiTransacctions.Datos.Models;
using WebApiTransacctions.Servicios.Interfaces;
using WebApiTransacctions.Repositorios.Clases;

namespace WebApiTransacctions.Controllers
{
    public class RateController : ApiController
    {
        private List<Rate> _listRates;
        private IReadJsonService<Rate> _jsonReader;
        private IJsonConverterApi<Rate> _jsonConverter;
        private RateRepository _repositorio;

        public RateController(IReadJsonService<Rate> jsonReader, IJsonConverterApi<Rate> jsonConverter, RateRepository repositorio) {
            _jsonConverter = jsonConverter;
            _jsonReader = jsonReader;
            _repositorio = repositorio;
        }

        // GET api/values
        public List<Rate> Get()
        {
            _listRates = _repositorio.Get();

            return _listRates;
        }

        // GET api/values/5
        public Rate Get(string to, string from)
        {

            var rate = _repositorio.Find(from, to);

            return rate;
        }

    }
}
