﻿using System.Collections.Generic;
using System.Web.Http;
using WebApiTransacctions.Datos.Models;
using WebApiTransacctions.Servicios.Interfaces;
using WebApiTransacctions.Repositorios.Clases;

namespace WebApiTransacctions.Controllers
{
    public class TransacctionController : ApiController
    {
        private List<Transacction> _listTransacctions;
        private IReadJsonService<Transacction> _jsonReader;
        private IJsonConverterApi<Transacction> _jsonConverter;
        private TransacctionRepository _repositorio;

        public TransacctionController(IReadJsonService<Transacction> jsonReader, IJsonConverterApi<Transacction> jsonConverter, TransacctionRepository repositorio)
        {
            _jsonConverter = jsonConverter;
            _jsonReader = jsonReader;
            _repositorio = repositorio;
        }

        // GET api/values
        public List<Transacction> Get()
        {
            _listTransacctions = _repositorio.Get();

            return _listTransacctions;
        }

        // GET api/values/5
        public Transacction Get(string sku)
        {

            var transacction = _repositorio.Find(sku);

            return transacction;
        }

    }
}
