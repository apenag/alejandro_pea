﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiTransacctions.Datos.Interfaces;

namespace WebApiTransacctions.Datos.Models
{
    public class Rate : IRate
    {

        public string _from { get; set; }
        public string _to { get; set; }
        public double _rate { get; set; }

        public Rate(string from,string to, double rate)
        {
            _from = from;
            _to = to;
            _rate = rate;
        }
    }
}