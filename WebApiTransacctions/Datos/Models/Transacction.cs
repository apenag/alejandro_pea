﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiTransacctions.Datos.Interfaces;

namespace WebApiTransacctions.Datos.Models
{
    public class Transacction : ITransacction
    {
        public string _sku { get; set; }
        public string _currency { get; set; }
        public decimal _amount { get; set; }

        public Transacction(string sku, string currency, decimal amount)
        {
            _sku = sku;
            _currency = currency;
            _amount = amount;
        }
    }
}