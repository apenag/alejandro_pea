﻿
namespace WebApiTransacctions.Datos.Interfaces
{
    public interface ITransacction
    {
        string _sku { get; set; }
        string _currency { get; set; }
        decimal _amount { get; set; }
    }
}