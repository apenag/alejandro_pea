﻿
namespace WebApiTransacctions.Datos.Interfaces
{
    public interface IRate
    {
        string _from { get; set; }
        string _to { get; set; }
        double _rate { get; set; }
    }
}
