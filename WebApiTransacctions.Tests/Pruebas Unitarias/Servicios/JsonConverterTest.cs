﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using WebApiTransacctions.Datos.Models;

namespace WebApiTransacctions.Tests.Pruebas_Unitarias.Servicios
{
    [TestClass]
    public class JsonConverterTest
    {
        [TestMethod]
        public void DeserializeTransacction()
        {
            string json= "[{ 'sku': 'T2006', 'amount': '10.00', 'currency': 'USD' },{ 'sku': 'M2007', 'amount': '34.57', 'currency': 'CAD' }, { 'sku': 'R2008', 'amount': '17.95', 'currency': 'USD' }, { 'sku': 'T2006', 'amount': '7.63', 'currency': 'EUR' }, { 'sku': 'B2009', 'amount': '21.23', 'currency': 'USD' }]";
            List<Transacction> listDeserializada = JsonConvert.DeserializeObject<List<Transacction>>(json);

            Assert.IsNotNull(listDeserializada);
        }

        [TestMethod]
        public void SerializeTransacction()
        {
            List<Transacction> list = new List<Transacction>();
            Transacction trans = new Transacction("T008","Prueba",10.00m);
            list.Add(trans);

            string json = JsonConvert.SerializeObject(list, Formatting.Indented);

            Assert.IsNotNull(json);
        }

        [TestMethod]
        public void DeserializeRate()
        {
            string json = "[{ 'from': 'EUR', 'to': 'USD', 'rate': '1.359' },{ 'from': 'CAD', 'to': 'EUR', 'rate': '0.732' },{ 'from': 'USD', 'to': 'EUR', 'rate': '0.736' },{ 'from': 'EUR', 'to': 'CAD', 'rate': '1.366' }]";
            List<Rate> listDeserializada = JsonConvert.DeserializeObject<List<Rate>>(json);

            Assert.IsNotNull(listDeserializada);
        }

        [TestMethod]
        public void SerializeRate()
        {
            List<Rate> list = new List<Rate>();
            Rate rate = new Rate("Prueba", "Prueba", 10.00);
            list.Add(rate);

            string json = JsonConvert.SerializeObject(list, Formatting.Indented);

            Assert.IsNotNull(json);
        }
    }
}
