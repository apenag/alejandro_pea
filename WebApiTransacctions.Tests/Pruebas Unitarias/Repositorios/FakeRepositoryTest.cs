﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApiTransacctions.Datos.Models;
using WebApiTransacctions.Repositorios.Clases;

namespace WebApiTransacctions.Tests.Pruebas_Unitarias.Repositorios
{
    [TestClass]
    public class FakeRepositoryTest
    {
        RateFakeRepository repositorioRate = new RateFakeRepository();
        TransacctionFakeRepository repositorioTransacction = new TransacctionFakeRepository();

        [TestMethod]
        public void FindRate()
        {
            string to = "EUR";
            string from = "USD";

            var obj = repositorioRate.Find(to, from);

            Assert.IsNotNull(obj);
        }

        [TestMethod]
        public void FindTransacction()
        {
            string sku = "T2006";

            var obj = repositorioTransacction.Find(sku);

            Assert.IsNotNull(obj);
        }

        [TestMethod]
        public void GetRates()
        {
            var obj = repositorioRate.Get();

            Assert.IsNotNull(obj);
        }

        [TestMethod]
        public void GetTransacctions()
        {
            var obj = repositorioTransacction.Get();

            Assert.IsNotNull(obj);
        }
    }
}

        
    

